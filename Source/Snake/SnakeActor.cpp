// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "SnakeElementBase.h"
#include "Interact.h"

// Sets default values
ASnakeActor::ASnakeActor()
{

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 65.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeActor::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		int num = SnakeElements.Num();
		if (num < 2)
		{
			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			SnakeElements.Add(NewSnakeElem);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Find(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
		else
		{
			auto LastElem = SnakeElements[num - 1];
			FVector LastElemLocation = LastElem->GetActorLocation();
			FTransform NewTransform(LastElemLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			SnakeElements.Add(NewSnakeElem);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Find(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}

		}
		/*SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Find(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}*/
	}
}

void ASnakeActor::Move()
{
	FVector MovementVector(ForceInitToZero);
		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
		}

		SnakeElements[0]->ToggleCollision();

			for (int i = SnakeElements.Num() - 1; i > 0; i--)
			{
				auto CurrentElem = SnakeElements[i];
				auto PreviousElem = SnakeElements[i - 1];
				FVector PrevLocation = PreviousElem->GetActorLocation();
				CurrentElem->SetActorLocation(PrevLocation);
			}

		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		SnakeElements[0]->ToggleCollision();
		ChangeMoveDirection = true;
}

void ASnakeActor::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteract* InteractableInterface = Cast<IInteract>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interaction(this, bIsFirst);
		}
	}
}

